import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Layout, Menu, Icon, Table } from 'antd';
import MainHead from '../lib/common/mainHead';
import Sidebar from '../lib/common/sidebar';

const { Header, Content, Footer, Sider } = Layout;

export default ({children}) => (<div>
    <Layout>
        <Sidebar />
        <Layout>
        <Header style={{ background: '#fff', padding: 0 }} />
        <Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
            <div style={{ minHeight: 675, padding: 24, textAlign: 'center' }}>
                
                {children}
            </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>
            Ant Design ©2018 Created by Ant UED
        </Footer>
    </Layout>
            <style>{`#components-layout-demo-fixed-sider .logo {
    height: 32px;
    background: rgba(255,255,255,.2);
    margin: 16px;
    }`}</style>
    </Layout>
</div>)