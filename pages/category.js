import React, { Component } from 'react';
import MainLayout from '../hocs/mainlayout';
import withData from '../lib/withData'
import Category from '../lib/components/category'

export default withData(() => (<MainLayout><Category/></MainLayout>));