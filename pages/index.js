import React, { Component } from 'react';
import MainLayout from '../hocs/mainlayout';
import withData from '../lib/withData'

import UserList from '../lib/components/userList';
import AddUser from '../lib/components/addUser';

export default withData(() => (<MainLayout><AddUser/><UserList /></MainLayout>));