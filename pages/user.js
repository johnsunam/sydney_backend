import React, { Component } from 'react';
import { Layout, Menu, Icon } from 'antd';

import mainLayout from '../hocs/mainlayout';
import Sidebar from '../lib/common/sidebar';

const { Header, Content, Footer, Sider } = Layout;

class User extends Component {
    constructor (props) {
        super(props)
        this.state = {
            collapsed: false,
        };
    }
    
    toggle = () => {
        this.setState({
          collapsed: !this.state.collapsed,
        });
    }

    render () {
        return  <Layout style={{ marginLeft: 200 }}>
        <Header style={{ background: '#fff', padding: 0 }} />
        <Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
            <div style={{ padding: 24, background: '#fff', textAlign: 'center' }}>
            ...
            <br />
            User list......
            </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>
            Ant Design ©2018 Created by Ant UED
        </Footer>
    </Layout>
    }
}

export default mainLayout( User )