import React, { Component } from 'react';
import Head from 'next/head'


class MainHead extends Component {
    constructor (props) {
        super(props)
        this.state = {
            stylePathAnt: '//cdnjs.cloudflare.com/ajax/libs/antd/3.11.2/antd.min.css'
        }
    }

    render () {
        return <div>
            <Head>
                <meta charSet='utf-8' />
                <link rel='stylesheet' href={this.state.stylePathAnt} />
            </Head>
        </div>
    }
}

export default MainHead