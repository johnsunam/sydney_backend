import React, { Component } from  'react';
import { Layout, Menu, Icon } from 'antd';
import Link from 'next/link'
const { Sider } = Layout;

class Sidebar extends Component {
    render () {
        return <Sider style={{ overflow: 'auto', height: '100vh', position: 'fixed', left: 0 }}>
        <div className="logo" style={{height: 32, background: 'white', margin: 16, align: 'center'}} >Sydney Admin</div>
        <Menu theme="dark" mode="inline" defaultSelectedKeys={['4']}>
            <Menu.Item key="1">
            <Link href="/">
            <span>
                <Icon type="user" theme="outlined" />
                <span className="nav-text">Users</span>
            </span>
            
            </Link>
            </Menu.Item>

            <Menu.Item key="2">
            <Link href="/category">
                <span>
                    <Icon type="video-camera" />
                    <span className="nav-text">Categories</span>
                </span>
            </Link>
            </Menu.Item>
            <Menu.Item key="3">
            <Link href="/dashboard">
                <span>
                    <Icon type="upload" />
                    <span className="nav-text">nav 3</span>
                </span>
            </Link>

            </Menu.Item>
            {/* <Menu.Item key="4">
            <Icon type="bar-chart" />
            <span className="nav-text">nav 4</span>
            </Menu.Item>
            <Menu.Item key="5">
            <Icon type="cloud-o" />
            <span className="nav-text">nav 5</span>
            </Menu.Item>
            <Menu.Item key="6">
            <Icon type="appstore-o" />
            <span className="nav-text">nav 6</span>
            </Menu.Item>
            <Menu.Item key="7">
            <Icon type="team" />
            <span className="nav-text">nav 7</span>
            </Menu.Item>
            <Menu.Item key="8">
            <Icon type="shop" />
            <span className="nav-text">nav 8</span>
            </Menu.Item> */}
        </Menu>
        </Sider>
    }
}

export default Sidebar;