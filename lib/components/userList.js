import React from 'react';
import { Layout, Menu, Icon, Table, Divider, Tag } from 'antd';
import {graphql} from 'react-apollo';
import gql from 'graphql-tag';

const { Header, Content, Footer, Sider } = Layout;


const UserList = ({data}) => {
    const { error, getUsers } = data;
    const columns = [
        {
            title: 'User Name',
            dataIndex: 'username',
            key: 'username'
        },
        {
            title: 'Address',
            dataIndex: 'address',
            key: 'address'
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email'
        },
        {
            title: 'Contact No',
            dataIndex: 'phone',
            key: 'phone'
        },
        {
            title: 'Role',
            dataIndex: 'role',
            key: 'role'
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) => (<span>
                <a href="javascript:;">Delete</a>
              </span>)
        }

    ]
    if(error) {
        return <div>Loading....</div>
    }
    if (getUsers) {
        return <Layout style={{ marginLeft: 200 }}>
        {/* <Header style={{ background: '#fff', padding: 0 }} /> */}
        <Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
            <div style={{ minHeight: 675, padding: 24, background: '#fff', textAlign: 'center' }}>
            <Table columns={columns} dataSource={getUsers} />
            </div>
        </Content>
        {/* <Footer style={{ textAlign: 'center' }}>
            Ant Design ©2018 Created by Ant UED
        </Footer> */}
    </Layout>
    }
    return <div>Loading.....</div>
}

export const getUsers = gql`
    query {
        getUsers { 
            _id,
            username, 
            address, 
            email,
            phone,
            role 
        }
    }
`

export default graphql(getUsers)(UserList);