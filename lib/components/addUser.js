import React, {Component} from 'react';
import {graphql} from 'react-apollo';
import gql from 'graphql-tag';
import {Button, Modal, Form, Input, Select} from 'antd';
import { getUsers } from './userList';
const FormItem = Form.Item;
const Option = Select.Option;

const CollectionCreateForm = Form.create()(
    // eslint-disable-next-line
    class extends React.Component {
      render() {
        const {
          visible, onCancel, onCreate, form,
        } = this.props;
        const { getFieldDecorator } = form;
        return (
          <Modal
            visible={visible}
            title="Create new User"
            okText="Create"
            onCancel={onCancel}
            onOk={onCreate}
          >
            <Form layout="vertical">
              <FormItem label="User Name">
                {getFieldDecorator('username', {
                  rules: [{ required: true, message: 'Please input the User Name of user!', type: 'string' }],
                })(
                  <Input />
                )}
              </FormItem>
              <FormItem label="Email">
                {getFieldDecorator('email', {
                  rules: [{ required: true, message: 'Please input the Email of user!', type: 'email' }],
                })(
                  <Input />
                )}
              </FormItem>
              <FormItem label="Address">
                {getFieldDecorator('address', {
                  rules: [{ required: false }],
                })(
                  <Input />
                )}
              </FormItem>
              <FormItem label="Contact No">
                {getFieldDecorator('phone', {
                  rules: [],
                })(
                  <Input type='number'/>
                )}
              </FormItem>
              <FormItem label="Choose role">
                    {getFieldDecorator('role', {
                        rules:[]
                    })(<Select
                        style={{ width: '100%' }}
                        placeholder="Please select"
                        // onChange={handleChange}
                      >
                        <Option value='sub-admin'>sub admin</Option>
                        <Option value='user'>user</Option>
                      </Select>)}
              </FormItem>
              {/* <FormItem className="collection-create-form_last-form-item">
                {getFieldDecorator('modifier', {
                  initialValue: 'public',
                })(
                  <Radio.Group>
                    <Radio value="public">Public</Radio>
                    <Radio value="private">Private</Radio>
                  </Radio.Group>
                )}
              </FormItem> */}
            </Form>
          </Modal>
        );
      }
    }
  );


class AddUser extends Component {
    constructor (props) {
        super(props)
        this.state = {
            visible: false
        }
    }

    showModal = () => {
        this.setState({visible: true});
    }

    handleCancel = () => {
        this.setState({ visible: false });
      }
    
      handleCreate = () => {
        const form = this.formRef.props.form;
        form.validateFields((err, values) => {
          if (err) {
            return;
          }
    
          console.log('Received values of form: ', values);
          this.props.addUser(values.email, values.address, values.username,  parseInt(values.phone), values.role)
          form.resetFields();
          this.setState({ visible: false });
        });
      }
    
      saveFormRef = (formRef) => {
        this.formRef = formRef;
      }

    render () {
        return <div>
        <Button type="primary" onClick = {this.showModal} >Add User</Button>
        <CollectionCreateForm
          wrappedComponentRef={this.saveFormRef}
          visible={this.state.visible}
          onCancel={this.handleCancel}
          onCreate={this.handleCreate}
        />
      </div>
    }
}

const addUser = gql`mutation ($data: UserInput!) { addUser( data: $data)
    {
        _id
        email 
        address 
        username
        phone
        role
    }}`;


export default graphql(addUser, {
    props: ({mutate}) =>({
        addUser: (email, address, username, phone, role) => 
        mutate({
            variables: {data:{email, address, username, phone, role}},
            update: (proxy, {data: {addUser}}) => {
                const data = proxy.readQuery({
                    query: getUsers 
                });
                console.log('///////adddUser',addUser, data)
                proxy.writeQuery({
                    query: getUsers,
                    data: {
                        ...data,
                        getUsers: [...data.getUsers, addUser]                 }
                })
            }
        })
    })
})(AddUser);