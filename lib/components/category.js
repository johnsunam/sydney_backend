import React from 'react';
import { Layout, Row, Col } from 'antd';
import ParentCategory from './parentCategory';
import ChildCategory from './childCategory';
const { Content } = Layout;

const category = () => {
    return (<Layout style={{ marginLeft: 200 }}>
        <Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
            <div style={{ minHeight: 675, padding: 24, background: '#fff', textAlign: 'center' }}>
                <Row type="flex" justify="center" align="top"><ParentCategory/></Row>
                {/* <Col span={12}>second</Col> */}
            </div>
        </Content>
       
    </Layout>)
}


export default category